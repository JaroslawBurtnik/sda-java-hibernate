package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"orderComplaintSet","orderDetailList"})
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    BigDecimal totalNetto;
    BigDecimal totalGross;
    String userEmail;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    // jedno zamowienie moze miec wiele pozycji
    // wlascicielem relacji bedzie order detail
    Set<OrderDetail> orderDetailList;

    @OneToOne(mappedBy = "order")
    OrderHistory orderHistory;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    // join table do laczenia przez tabele dodatkowa
    @JoinTable(
            name = "tabela_order_order_history",
            // joinColums nazwa kolumny w tabeli dodatkowej z kluczem do tabeli laczonej + nazwa pola w encji z kluczem
            // po ktorym laczymy
            joinColumns = @JoinColumn(name = "order_complaint_id", referencedColumnName = "id"),
            // nazwa kolumny z kluczem glownym z encji Order
            inverseJoinColumns = @JoinColumn(name = "order_id")
    )
    Set<OrderComplaint> orderComplaintSet;

    public Order(BigDecimal totalGross, String userEmail) {

        this.totalGross = totalGross;
        this.userEmail = userEmail;
    }

    public void addOrderDetail(OrderDetail orderDetail) {
        orderDetail.setOrder(this);
        orderDetailList.add(orderDetail);
    }
}
